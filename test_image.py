import os
import gdown
import torch
import argparse
from PIL import Image

import src.configs.gan_config as cfg
from src.utils import convert_image
from src.models.model import Generator

parser = argparse.ArgumentParser(description='Test Single Image')
parser.add_argument('--image_name', type=str, required=True, help='low resolution image name')
parser.add_argument('--generator_weights_path', default=cfg.generator_weights_path, type=str)
parser.add_argument('--device', default='cpu', type=str, choices=['cuda', 'cpu'], help='using cuda or cpu')
args = parser.parse_args()


def main():
    if not os.path.exists(args.generator_weights_path):
        gdown.download(url=cfg.generator_weight_url, output=args.generator_weights_path, quiet=False, fuzzy=True)

    generator = Generator().eval()
    generator.load_state_dict(torch.load(args.generator_weights_path, map_location=args.device))

    image = Image.open(args.image_name, mode="r").convert('RGB')

    sr_img_srgan = generator(convert_image(image, source='pil', target='-norm').unsqueeze(0).to(args.device))
    sr_img_srgan = sr_img_srgan.squeeze(0).cpu().detach()
    sr_img = convert_image(sr_img_srgan, source='[-1, 1]', target='pil')
    dir_path, image_name = os.path.split(args.image_name.replace('/', '\\'))
    sr_img.save(os.path.join(dir_path, f'sr_x4_{image_name}'))


if __name__ == "__main__":
    main()
