from PIL import Image
import torchvision.transforms.functional as F

import src.configs.gan_config as cfg
from src.models.model import Generator


def convert_image(image: Image, source: str, target: str) -> Image:
    assert source in {'pil', '[0, 1]', '[-1, 1]'}, "Cannot convert from source format %s!" % source

    if source == 'pil':
        image = F.to_tensor(image)
    elif source == '[-1, 1]':
        image = (image + 1.) / 2.

    if target == 'pil':
        image = F.to_pil_image(image)
    elif target == '[0, 255]':
        image = 255. * image
    elif target == '[-1, 1]':
        image = 2. * image - 1.
    return image


def visualize(image: Image, srgan_generator: Generator) -> Image:
    hr_img = Image.open(image, mode="r").convert('RGB')
    lr_img = hr_img.resize((int(hr_img.width / 4), int(hr_img.height / 4)))

    sr_img_srgan = srgan_generator(
        convert_image(
            lr_img,
            source='pil',
            target='-norm'
        ).unsqueeze(0).to(cfg.device))
    sr_img_srgan = sr_img_srgan.squeeze(0).cpu().detach()
    sr_img_srgan = convert_image(sr_img_srgan, source='[-1, 1]', target='pil')
    return sr_img_srgan
