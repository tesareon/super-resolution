import os
import gdown
import torch
import mlflow
import argparse
import torch.nn as nn
from tqdm.notebook import tqdm
import matplotlib.pyplot as plt
import src.configs.gan_config as cfg
from torchvision.utils import save_image
from torchvision.utils import make_grid
from torch.utils.data import DataLoader
from mlflow.models.signature import infer_signature
from src.data.dataset import MyImageFolder
from vgg_features import TruncatedVGG19
from model import Generator, Discriminator

parser = argparse.ArgumentParser(description='Train SRGAN')
parser.add_argument('--epochs', default=100, type=int)
parser.add_argument('--dataset_path', type=str, required=True, )
parser.add_argument('--image_size', default=96, type=int)
parser.add_argument('--batch_size', default=64, type=int)
parser.add_argument('--lr', default=0.0001, type=str, help='learning rate')
parser.add_argument('--pretrained_weights', default=False, type=bool)
parser.add_argument('--pretrained_weights_path', default='models', type=str)
parser.add_argument('--device', default='cpu', type=str, choices=['cuda', 'cpu'])
args = parser.parse_args()

mlflow.set_tracking_uri("http://213.202.219.36:5000")
mlflow.set_experiment("SRGAN")
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://213.202.219.36:9000"


def train(
        epochs,
        train_loader,
        generator,
        discriminator,
        truncated_vgg19,
        content_loss_criterion,
        adversarial_loss_criterion,
        optimizer_g,
        optimizer_d,
        device
):
    g_losses = []
    d_losses = []

    for epoch in tqdm(range(epochs)):

        generator.train()
        discriminator.train()

        for i, (lr_imgs, hr_imgs) in enumerate(train_loader):
            lr_imgs = lr_imgs.to(device)
            hr_imgs = hr_imgs.to(device)

            sr_imgs = generator(lr_imgs)

            sr_imgs_in_vgg_space = truncated_vgg19(sr_imgs)
            hr_imgs_in_vgg_space = truncated_vgg19(hr_imgs).detach()

            sr_discriminated = discriminator(sr_imgs)

            content_loss = content_loss_criterion(sr_imgs_in_vgg_space, hr_imgs_in_vgg_space)
            adversarial_loss = adversarial_loss_criterion(sr_discriminated, torch.ones_like(sr_discriminated))
            perceptual_loss = content_loss + 1e-3 * adversarial_loss

            optimizer_g.zero_grad()
            perceptual_loss.backward()

            optimizer_g.step()

            hr_discriminated = discriminator(hr_imgs)
            sr_discriminated = discriminator(sr_imgs.detach())

            adversarial_loss = adversarial_loss_criterion(sr_discriminated, torch.zeros_like(sr_discriminated)) + \
                adversarial_loss_criterion(hr_discriminated, torch.ones_like(hr_discriminated))

            optimizer_d.zero_grad()
            adversarial_loss.backward()

            optimizer_d.step()

            g_losses.append(perceptual_loss.item())
            d_losses.append(adversarial_loss.item())

        if epoch == epochs - 1:
            save_samples(generator, epoch + 1, lr_imgs, show=True)

        mlflow.log_metrics({'generator_loss': sum(g_losses) / len(g_losses),
                            'discriminator_loss': sum(d_losses) / len(d_losses)},
                           step=epoch)

        state_dict = {
            'epoch': epoch,
            'generator': generator.state_dict(),
            'discriminator': discriminator.state_dict(),
            'optimizer_g': optimizer_g.state_dict(),
            'optimizer_d': optimizer_d.state_dict()
        }
        torch.save(state_dict, 'models/checkpoint_srgan.pth.tar')
        mlflow.log_artifact('models/checkpoint_srgan.pth.tar')

    generator.eval()
    signature = infer_signature(lr_imgs.cpu().numpy(), generator(lr_imgs).detach().cpu().numpy())
    mlflow.pytorch.log_model(generator, 'model', signature=signature)


def show_images(images, nmax=16):
    fig, ax = plt.subplots(figsize=(4, 4))
    ax.set_xticks([])
    ax.set_yticks([])
    ax.imshow(make_grid(images.detach()[:nmax], nrow=4).permute(1, 2, 0))


def show_batch(dl, nmax=16):
    for images, _ in dl:
        show_images(images, nmax)
        break


def save_samples(gen, index, latent_tensors, show=True):
    fake_images = gen(latent_tensors)
    fake_fname = 'generated-images-{0:0=4d}.png'.format(index)
    save_image(fake_images, fake_fname, nrow=4)
    print('Saving', fake_fname)
    if show:
        fig, ax = plt.subplots(figsize=(4, 4))
        ax.set_xticks([])
        ax.set_yticks([])
        ax.imshow(make_grid(fake_images.cpu().detach(), nrow=4).permute(1, 2, 0))


def main():
    data = MyImageFolder(
        args.dataset_path,
        resolution=args.image_size
    )
    dataloader = DataLoader(data, batch_size=args.batch_size)

    discriminator = Discriminator(
        args.image_size,
        args.image_size
    ).to(args.device)
    generator = Generator().to(args.device)

    content_loss_criterion = nn.MSELoss()
    adversarial_loss_criterion = nn.BCEWithLogitsLoss()

    content_loss_criterion = content_loss_criterion.to(args.device)
    adversarial_loss_criterion = adversarial_loss_criterion.to(args.device)

    optimizer_d = torch.optim.Adam(
        discriminator.parameters(),
        lr=args.lr,
        betas=cfg.model_betas
    )
    optimizer_g = torch.optim.Adam(
        generator.parameters(),
        lr=args.lr,
        betas=cfg.model_betas
    )

    if args.pretrained_weights:
        if not os.path.exists(args.pretrained_weights_path):
            output = args.pretrained_weights_path
            gdown.download(url=cfg.weights_url, output=output, quiet=False, fuzzy=True)

        checkpoint = torch.load(args.pretrained_weights_path)
        generator.load_state_dict(checkpoint['generator'])
        optimizer_g.load_state_dict(checkpoint['optimizer_g'])

        discriminator.load_state_dict(checkpoint['discriminator'])
        optimizer_d.load_state_dict(checkpoint['optimizer_d'])

    vgg19_i = 5
    vgg19_j = 4

    truncated_vgg19 = TruncatedVGG19(i=vgg19_i, j=vgg19_j)
    truncated_vgg19.eval()
    truncated_vgg19 = truncated_vgg19.to(args.device)

    with mlflow.start_run():
        mlflow.log_params({
            'epochs': args.epochs,
            'batch_size': args.batch_size,
            'learning_rate': args.lr,
        })

        train(
            args.epochs,
            dataloader,
            generator,
            discriminator,
            truncated_vgg19,
            content_loss_criterion,
            adversarial_loss_criterion,
            optimizer_g,
            optimizer_d,
            args.device
        )


if __name__ == "__main__":
    main()
